<?
Class test extends CModule
{
    var $MODULE_ID = "test";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function test()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "test";
        $this->MODULE_DESCRIPTION = "test";
    }

    function DoInstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION,$DB;
        // Install events

		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/test/install/components",
             $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);


		$arrErrors = $DB->RunSqlBatch(__DIR__."/data/install.sql");

        RegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Установка модуля dull", $DOCUMENT_ROOT."/bitrix/modules/dull/install/step.php");
        return true;
    }

    function DoUninstall()
    {
        global $DOCUMENT_ROOT, $APPLICATION,$DB;
		$DB->Query("DROP TABLE test_test");

        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile("Деинсталляция модуля dull", $DOCUMENT_ROOT."/bitrix/modules/dull/install/unstep.php");
        return true;
    }
}