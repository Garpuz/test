<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<form method="post" id="form_1290">
	<input name="NAME" placeholder="<?=GetMessage("TEST_NAME")?>" >
	<input name="SONAME" placeholder="<?=GetMessage("TEST_SONAME")?>" >
	<input name="TEL" placeholder="<?=GetMessage("TEST_TEL")?>" >
	<input vlue="<?=GetMessage("TEST_SUBMIT")?>" type="submit">
</form>

<table id="table_1290" width="100%" border="1">
	<tr>
		<td><?=GetMessage("TEST_NAME")?></td>
		<td><?=GetMessage("TEST_SONAME")?></td>
		<td><?=GetMessage("TEST_TEL")?></td>
	</tr>

	<?foreach($arResult as $item){?>
	
	<tr>
		<td><?=$item['NAME']?></td>
		<td><?=$item['SONAME']?></td>
		<td><?=$item['TEL']?></td>
	</tr>

	<?}?>
</table>

<script>

$(document).ready(function () {
	$('#form_1290').SendForm();
});

</script>