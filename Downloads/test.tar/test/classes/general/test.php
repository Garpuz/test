<?php
class test {

	function exsistTable(){
		global $DB;
		$query = "SHOW tables like 'test_test'";
		$res = $DB->Query($query); 
		$result  = $res->Fetch();
		return $result ;
	}


	function insertTable($data){
		global $DB;

        $arFields = array(
            "NAME"   => "'".trim($data['NAME'])."'",
            "SONAME" => "'".trim($data['SONAME'])."'",
            "TEL"    => "'".intval($data['TEL'])."'",
            );


		$DB->StartTransaction();
		$ID = $DB->Insert("test_test", $arFields, $err_mess.__LINE__);
		if ($ID) 
				{
					$DB->Commit();
					return $data;
				}

	}

	function selectTable(){
		global $DB;
    $strSql = "
        SELECT * FROM test_test  ";
    $res = $DB->Query($strSql, false, $err_mess.__LINE__);
		while($result = $res->Fetch()){
			$arResult[] = $result;
		}
    	return $arResult;
	}

	function ajax($post){
		$respon = $this->insertTable($post);
		$GLOBALS['APPLICATION']->RestartBuffer();
		return $respon;
	}

	function loadPage(){
		$arResult = $this->selectTable();
		return $arResult;
	}

}